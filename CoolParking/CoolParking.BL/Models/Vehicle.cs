﻿using System;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public static readonly string IdRegex = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

        [JsonPropertyName("id")]
        public string Id { get; }
        
        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; }
        
        [JsonPropertyName("balance")]
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (Regex.IsMatch(id, IdRegex) == false)
            {
                throw new ArgumentException();
            }

            if (balance < 0)
            {
                throw new ArgumentException();
            }

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public override string ToString()
        {
            return $"{Id} {VehicleType} {Balance}";
        }

        private static string GenerateRandomRegistrationPlateNumber()
        {
            int firstPartLength = 2;
            int secondPartLength = 4;
            int thirdPartLength = 2;

            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string digits = "0123456789";

            string registrationPlateNumber = GenerateRandomStringFromDictionary(firstPartLength, letters) + "-" +
                                             GenerateRandomStringFromDictionary(secondPartLength, digits) + "-" +
                                             GenerateRandomStringFromDictionary(thirdPartLength, letters);

            return registrationPlateNumber;
        }

        private static string GenerateRandomStringFromDictionary(int destStringLenght, string dictionary)
        {
            Random random = new Random();

            StringBuilder resultStringBuilder = new StringBuilder();
            int dictionaryRange = dictionary.Length;

            for (int i = 0; i < destStringLenght; i++)
            {
                resultStringBuilder.Append(dictionary[random.Next(dictionaryRange)]);
            }

            return resultStringBuilder.ToString();
        }
    }
}
