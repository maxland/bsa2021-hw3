﻿using System;
using System.Text.Json.Serialization;
namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {        
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }
        
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }

        [JsonPropertyName("transactionDate")]
        public DateTime Time { get; set; }

        public TransactionInfo(DateTime transactionDate, string vehicleId, decimal sum)
        {
            Time = transactionDate;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            return string.Format("{0:HH:mm:ss} {1} {2}", Time, VehicleId, Sum);
        }
    }
}