﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Collections.Generic;
using CoolParking.BL.Models;
using System.Text;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CoolParking.CUI
{
    public class ConsoleUserInterface
    {
        private readonly HttpClient _client;
        private const string DefaultUri = "https://localhost:5001/";

        public ConsoleUserInterface()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(DefaultUri);
        }

        public void Execute()
        {
            char key = '\0';
            while (key != 'q')
            {
                DisplayMainMenu();
                key = Console.ReadKey().KeyChar;
                Console.Clear();

                switch (key)
                {
                    case 'q':
                        return ;
                    case '1':
                        DisplayParkingBalance();
                        break;
                    case '2':
                        DisplayFreePlaces();
                        break;
                    case '3':
                        DisplayCurrentPeriodTransactions();
                        break;
                    case '4':
                        DisplayTransactionsHistory();
                        break;
                    case '5':
                        DisplayVehicle();
                        break;
                    case '6':
                        DisplayAllVehicles();
                        break;
                    case '7':
                        DisplayAddVehicleMenu();                     
                        break;
                    case '8':
                        DisplayRemoveVehicleMenu();                        
                        break;
                    case '9':
                        DisplayTopUpVehocleMenu();
                        break;
                }

                WaitResults();
            }
        }

        private void DisplayMainMenu()
        {
            Console.WriteLine("Choose the option: ");
            Console.WriteLine("[1] - Show current Parking balance");
            Console.WriteLine("[2] - Show the number of free parking places");
            Console.WriteLine("[3] - Show all Parking Transactions for the current period");
            Console.WriteLine("[4] - Show transaction history");
            Console.WriteLine("[5] - Show the Vehicle");
            Console.WriteLine("[6] - Show all Vehicles in the Parking");
            Console.WriteLine("[7] - Put the Vehicle in the Parking");
            Console.WriteLine("[8] - Pick up the Vehicle from the Parking");
            Console.WriteLine("[9] - Top up Vehicle balance\n");
            Console.WriteLine("[q] - Quit");
        }

        private void WaitResults()
        {
            Console.WriteLine("Wait for results and then press any key to back to menu");
            Console.ReadKey();
            Console.Clear();
        }

        private async void DisplayParkingBalance()
        {
            var response = await _client.GetAsync("api/parking/balance");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                decimal balance = JsonSerializer.Deserialize<decimal>(responseBody);
                Console.WriteLine($"\nParking balance: {balance}");
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private async void DisplayFreePlaces()
        {
            var capacityResponse = await _client.GetAsync("api/parking/capacity");
            var freePlacesResponse = await _client.GetAsync("api/parking/freePlaces");

            if (capacityResponse.StatusCode == HttpStatusCode.OK &&
                freePlacesResponse.StatusCode == HttpStatusCode.OK)
            {
                string responseBodyCapacity = await capacityResponse.Content.ReadAsStringAsync();
                int capacity = JsonSerializer.Deserialize<int>(responseBodyCapacity);

                string responseBodyFreePlaces = await freePlacesResponse.Content.ReadAsStringAsync();
                int freePlaces = JsonSerializer.Deserialize<int>(responseBodyFreePlaces);

                Console.WriteLine($"\n{freePlaces}/{capacity} parking places are available");
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private async void DisplayCurrentPeriodTransactions()
        {
            var response = await _client.GetAsync("api/transactions/last");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                var lastTransactions = JsonSerializer.Deserialize<TransactionInfo[]>(responseBody);

                Console.WriteLine("\nAll transactions for the current period:");
                foreach (TransactionInfo transactionInfo in lastTransactions)
                {
                    Console.WriteLine(transactionInfo.ToString());
                }
            }
        }

        private async void DisplayTransactionsHistory()
        {
            var response = await _client.GetAsync("api/transactions/all");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string transactionsHistory = await response.Content.ReadAsStringAsync();

                Console.WriteLine("Transactions log:");
                Console.Write(transactionsHistory);
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private async void DisplayVehicle()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            var response = await _client.GetAsync("api/vehicles/" + vehicleId);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                Console.WriteLine("Id is invalid");
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                Console.WriteLine($"Vehicle with ID {vehicleId} does not exist");
            }
            else if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                VehicleRecord vehicle = JsonSerializer.Deserialize<VehicleRecord>(responseBody);
                Console.WriteLine(vehicle.ToString());
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private async void DisplayAllVehicles()
        {
            var response = await _client.GetAsync("api/vehicles");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                var vehicles = JsonSerializer.Deserialize<IEnumerable<VehicleRecord>>(responseBody);

                Console.WriteLine("Vehicles in the Parking:");
                foreach (VehicleRecord vehicle in vehicles)
                {
                    Console.WriteLine(vehicle.ToString());
                }
            }
            else
            {
                Console.WriteLine("Server error");
            }

        }

        private async void DisplayAddVehicleMenu()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            int type;
            InputVehicleTypeMenu(out type);
            
            decimal startBalance;
            InputSumMenu(out startBalance);

            VehicleRecord vehicleRecord = new VehicleRecord() { Id = vehicleId, Type = type, Balance = startBalance };

            var options = new JsonSerializerOptions { IncludeFields = true };
            var response = await _client.PostAsJsonAsync("api/vehicles", vehicleRecord, options);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                Console.WriteLine("Invalid input");
            }
            else if (response.StatusCode == HttpStatusCode.Created)
            {
                Console.WriteLine("Vehicel successfully created:");

                string responseBody = await response.Content.ReadAsStringAsync();
                VehicleRecord responseVehicle = JsonSerializer.Deserialize<VehicleRecord>(responseBody);

                Console.WriteLine(responseVehicle.ToString());
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private async void DisplayRemoveVehicleMenu()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            var response = await _client.DeleteAsync("api/vehicles/" + vehicleId);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                Console.WriteLine("Id is invalid");
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                Console.WriteLine($"Vehicle with ID {vehicleId} does not exist");
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                Console.WriteLine("Vehicel successfully removed");
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private async void DisplayTopUpVehocleMenu()
        {
            string vehicleId;
            InputVehicleIdMenu(out vehicleId);

            decimal sum;
            InputSumMenu(out sum);

            var tupleTopUp = new { id = vehicleId, Sum = sum};

            var options = new JsonSerializerOptions { IncludeFields = true };
            var response = await _client.PutAsJsonAsync("api/transactions/topUpVehicle", tupleTopUp, options);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                Console.WriteLine("Invalid input");
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                Console.WriteLine($"Vehicle with ID {vehicleId} does not exist");
            }
            else if (response.StatusCode == HttpStatusCode.OK)
            {
                string responseBody = await response.Content.ReadAsStringAsync();
                VehicleRecord vehicle = JsonSerializer.Deserialize<VehicleRecord>(responseBody);

                Console.WriteLine("Vehicel successfully top upped");
                Console.WriteLine(vehicle.ToString());
            }
            else
            {
                Console.WriteLine("Server error");
            }
        }

        private void InputVehicleIdMenu(out string stringForInput)
        {
            Console.WriteLine("Enter the vehicle ID in the format XX-YYYY-XX, where");
            Console.WriteLine("X - English uppercase letter, Y - decimal digit");
            stringForInput = Console.ReadLine();
            Console.Clear();
        }

        private void InputVehicleTypeMenu(out int numForInput)
        {
            while (true)
            {
                Console.WriteLine("Choose the vehicle type:");
                Console.WriteLine("[0] - Passenger car");
                Console.WriteLine("[1] - Truck");
                Console.WriteLine("[2] - Bus");
                Console.WriteLine("[3] - Motorcycle");

                bool isInputValid = int.TryParse(Console.ReadLine(), out numForInput);

                if (isInputValid)
                {
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Please try again");
                }
            }

            Console.Clear();
        }

        private void InputSumMenu(out decimal numberForInput)
        {
            while (true)
            {
                Console.WriteLine("Enter the sum:");
                bool isInputValid = decimal.TryParse(Console.ReadLine(), out numberForInput);

                if (isInputValid)
                {
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input. Please try again");
                }
            }
            
            Console.Clear();
        }
    }
}
