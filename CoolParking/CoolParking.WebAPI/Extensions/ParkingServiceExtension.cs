﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    public static class ParkingServiceExtension
    {
        public static Vehicle GetVehicleById(this IParkingService parkingService, string id)
        {
            var vehicles = parkingService.GetVehicles();

            Vehicle vehicle = null;
            foreach (Vehicle v in vehicles)
            {
                if (v.Id == id)
                {
                    vehicle = v;
                    break;
                }
            }

            return vehicle;
        }
    }
}
