﻿using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Extensions
{
    public record TopUpRecord
    {
        [JsonPropertyName("id")]
        public string Id { get; init; }

        [JsonPropertyName("Sum")]
        public decimal Sum { get; init; }
    }
}
