﻿using System;
using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public string LogPath { get; }

        public string Read()
        {
            if (File.Exists(LogPath) == false)
            {
                throw new InvalidOperationException();
            }

            StringBuilder resultStringBuilder = new StringBuilder();
            using (var file = new StreamReader(LogPath))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    resultStringBuilder.Append(line);
                    resultStringBuilder.Append('\n');
                }
            }

            return resultStringBuilder.ToString();
        }

        public void Write(string logInfo)
        {
            using (var file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}