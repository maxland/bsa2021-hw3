﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        readonly IParkingService _parkingService;

        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/parking/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        // GET: api/parking/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        // GET: api/parking/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}
