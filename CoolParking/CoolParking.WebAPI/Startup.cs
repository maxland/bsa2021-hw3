using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        readonly string filename = $@"{ Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) }\Transactions.log";

        readonly IParkingService _parkingService;
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;

        public Startup(IConfiguration configuration)
        {
            _withdrawTimer = new TimerService(Settings.PaymentChargePeriod);
            _logTimer = new TimerService(Settings.LoggingPeriod);
            _logService = new LogService(filename);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
            
            _withdrawTimer.Start();
            _logTimer.Start();

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(_parkingService);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "ParkingApi",
                    pattern: "api/parking/{action}");

                endpoints.MapControllerRoute(
                    name: "VehiclesApi",
                    pattern: "api/vehicles/{id?}");

                endpoints.MapControllerRoute(
                    name: "TransactionsApi",
                    pattern: "api/transactions/{action}");
            });
        }
    }
}
