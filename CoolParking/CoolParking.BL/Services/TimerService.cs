﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;
        
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(int seconds)
        {
            Interval = seconds * 1000;
            _timer = new Timer(Interval);
            _timer.AutoReset = true;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.Elapsed += Elapsed;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}