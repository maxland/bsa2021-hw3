﻿using Microsoft.AspNetCore.Mvc;
using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        readonly IParkingService _parkingService;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/vehicles
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            return Ok(_parkingService.GetVehicles());
        }

        // GET api/vehicles/AA-0001-AA
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            if (Regex.IsMatch(id, Vehicle.IdRegex) == false)
            {
                return new BadRequestResult();
            }

            Vehicle vehicle = _parkingService.GetVehicleById(id);

            if (vehicle == null)
            {
                return new NotFoundResult();
            }

            return Ok(vehicle);
        }

        // POST api/vehicles
        [HttpPost]
        public ActionResult<Vehicle> Post([FromBody] Vehicle vehicle)
        {
            try
            {
                _parkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException)
            {
                return new BadRequestResult();
            }
            catch (InvalidOperationException)
            {
                return new BadRequestResult();
            }

            return Created("api/vehicles", vehicle);
        }

        // DELETE api/vehicles/AA-0001-AA
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            if (Regex.IsMatch(id, Vehicle.IdRegex) == false)
            {
                return new BadRequestResult();
            }

            Vehicle vehicle = _parkingService.GetVehicleById(id);

            if (vehicle == null)
            {
                return new NotFoundResult();
            }

            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (InvalidOperationException)
            {
                return new BadRequestResult();
            }

            return NoContent();
        }
    }
}
