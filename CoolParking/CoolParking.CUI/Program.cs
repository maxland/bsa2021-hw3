﻿using System;

namespace CoolParking.CUI
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleUserInterface cui = new ConsoleUserInterface();
            cui.Execute();
        }
    }
}
