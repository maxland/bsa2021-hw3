﻿using System.Text.Json.Serialization;
using CoolParking.BL.Models;

namespace CoolParking.CUI
{
    record VehicleRecord
    {
        [JsonPropertyName("id")]
        public string Id { get; init; }

        [JsonPropertyName("vehicleType")]
        public int Type { get; init; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; init; }

        public override string ToString()
        {
            VehicleType type;

            switch (Type)
            {
                case 0:
                    type = VehicleType.PassengerCar;
                    break;
                case 1:
                    type = VehicleType.Truck;
                    break;
                case 2:
                    type = VehicleType.Bus;
                    break;
                case 3:
                    type = VehicleType.Motorcycle;
                    break;
                default:
                    type = default;
                    break;
            }

            return $"{Id} {type} {Balance}";
        }
    }
}
