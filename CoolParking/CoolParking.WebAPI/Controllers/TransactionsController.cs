﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using CoolParking.WebAPI.Extensions;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        readonly IParkingService _parkingService;

        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/transactions/last
        [HttpGet("last")]
        public ActionResult<TransactionInfo[]> GetLast()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        // GET api/transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {
            return Ok(_parkingService.ReadFromLog());
        }

        // PUT api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> Put([FromBody] TopUpRecord request)
        {
            if (Regex.IsMatch(request.Id, Vehicle.IdRegex) == false ||
                request.Sum < 0)
            {
                return new BadRequestResult();
            }

            Vehicle vehicle = _parkingService.GetVehicleById(request.Id);

            if (vehicle == null)
            {
                return new NotFoundResult();
            }

            try
            {
                _parkingService.TopUpVehicle(request.Id, request.Sum);
            }
            catch (ArgumentException)
            {
                return new BadRequestResult();
            }

            return Ok(vehicle);
        }
    }
}
